import config from "config";
import etherscan_csv from './etherscan-csv';

(async function main() {
  console.log(await etherscan_csv.save_source_from_csv('./src/etherscan-csv.ts', config));
})();

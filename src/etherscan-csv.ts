import fs, {PathLike} from 'fs';
import neat_csv, {Row} from 'neat-csv';
import {IConfig} from 'config';
import axios from 'axios';

function build_chunks(array: string[], size: number): string[][] {
  return Array.apply(0, new Array(Math.ceil(array.length / size)))
    .map((_, index) => array.slice(index * size, (index + 1) * size))
}

async function slow_call(func: Function, contracts: string[], apikey: any, interval: number, size: number) {
  let chunks = build_chunks(contracts, size);
  for (const chunk of chunks) {
    await new Promise(resolve => setTimeout(() => {
      chunk.map(contract => func(contract, apikey)); 
      resolve();
    }, interval));
  }
}

async function get_contract(contract: string, apikey: string, action: string) {
  return axios({
    method: 'get',
    url: `https://api.etherscan.io/api?module=contract&action=${action}&address=${contract}&apikey=${apikey}`
  });
}

export async function get_and_save_source(contract: string, apikey: any) {
  const file = `./contracts/${contract}.sol`
  fs.promises.access(file, fs.constants.R_OK | fs.constants.W_OK)
    .then(() => console.log(`${file} already exists, not written.`))
    .catch(async () => {
      get_contract(contract, apikey, 'getsourcecode')
      .then(response => fs.promises.writeFile(file, response.data.result[0].SourceCode))
      .then(() => console.log(`saved ${file}`))
      .catch(console.log)
    });
}

function get_contracts_from_csv(data: Row[]): string[] {
  return data.map(row => row[1]).slice(1,);
}

export async function save_source_from_csv(csv: PathLike, config: IConfig) {
  fs.readFile(csv, async (err, data) => {
    if (err) {
      console.error(err)
      return
    }

    const apikey = config.get('api.etherscan.key');
    const interval: number = config.get('api.etherscan.interval');
    const chunk_size: number = config.get('api.etherscan.chunk_size');

    await neat_csv(data)
      .then(get_contracts_from_csv)
      .then(contracts => slow_call(get_and_save_source, contracts, apikey, interval, chunk_size));
  });
}

export default {
  "save_source_from_csv": save_source_from_csv
}

# DAppscan Discovery

This is the data scraper for DAppscan that finds DApps to scan

To add your own API keys and csv paths create a new file `config/local.json`
with the contents:

```json
{
    "api": {
        "etherscan": {
            "key": "your-key-here",
            "csv": "./etherscan-export.csv"
        }
    }
}
```

To get a CSV list of Verified contract addresses from Etherscan go to
https://etherscan.io/exportData?type=open-source-contract-codes

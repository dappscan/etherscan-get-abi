FROM alpine:latest
RUN apk update
RUN apk add nodejs

RUN mkdir /app
VOLUME . /app

CMD ["npm", "run", "start"]
